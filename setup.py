import setuptools

with open("README.md") as readme:
    long_description = readme.read()

setuptools.setup(
    name="getayeh",
    version="0.1.0",
    description="Print random ayeh from holy quran",
    long_description=long_description,
    long_description_content_type="text/markdown",
    author="Seyed Hosein Mousavi Fard",
    author_email="shmf1385@protonmail.com",
    keywords="quran",
    url="https://codeberg.org/shmf1385/getayeh",
    classifiers=[],
    packages=["getayeh"],
    package_dir={"getayeh": "src"},
    include_package_data=True,
    scripts=[
        "bin/getayeh"
    ],
)
